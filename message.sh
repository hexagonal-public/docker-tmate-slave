#!/bin/sh

RSA=$(ssh-keygen -l \
                 -f /etc/tmate-keys/ssh_host_rsa_key \
                 -E md5 2>&1 \
        | cut -d\  -f 2 \
        | sed s/MD5://)
ECDSA=$(ssh-keygen -l \
                   -f /etc/tmate-keys/ssh_host_ecdsa_key \
                   -E md5 2>&1 \
          | cut -d\  -f 2 \
          | sed s/MD5://)
ED25519=$(ssh-keygen -l \
                     -f /etc/tmate-keys/ssh_host_ed25519_key \
                     -E md5 2>&1 \
            | cut -d\  -f 2 \
            | sed s/MD5://)
echo Add this to your ~/.tmate.conf file
echo set -g tmate-server-host ${HOST:-<server ip/hostname>}
echo set -g tmate-server-port ${PORT:-<server port>}
if [[ -n ${RSA} ]]
then
  echo set -g tmate-server-rsa-fingerprint   \"$RSA\"
fi
if [[ -n ${ECDSA} ]]
then
  echo set -g tmate-server-ecdsa-fingerprint   \"$ECDSA\"
fi
if [[ -n ${ED25519} ]] # ed25519 do not seem to be supported client-side yet, but should work in the future
then
  echo set -g tmate-server-ed25519-fingerprint   \"$ED25519\"
fi
echo set -g tmate-identity \"\"              # Can be specified to use a different SSH key.

touch /tmp/message.sh
