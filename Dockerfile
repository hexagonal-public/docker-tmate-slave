FROM alpine:edge
MAINTAINER Louis Kottmann <lkottmann@hexagonalconsulting.com>

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && apk update \
    && apk upgrade \
    && apk add --no-cache \
        libc6-compat \
        msgpack-c \
        ncurses-libs \
        libevent

# tmate build dependencies
RUN apk add --no-cache --virtual build-dependencies \
        build-base \
        ca-certificates \
        bash \
        wget \
        git \
        openssh \
        libc6-compat \
        automake \
        autoconf \
        zlib-dev \
        libevent-dev \
        msgpack-c-dev \
        ncurses-dev \
        libexecinfo-dev

ADD backtrace.patch /backtrace.patch
ADD message.sh /tmp/message.sh
ADD tmate-slave.sh /tmate-slave.sh

# use patched version to create keys
#   because we rollback to using ECDSA keys until ED25519 ones are functional
ADD create_keys.sh /tmp/create_keys.sh

RUN mkdir /src \
    && git clone https://github.com/tmate-io/tmate-slave.git /src/tmate-server \
    && cd /src/tmate-server \
    && git apply /backtrace.patch \
    && mv /tmp/create_keys.sh ./create_keys.sh \
    && ./create_keys.sh \
    && mv keys /etc/tmate-keys

# libssh build dependencies
RUN apk add --no-cache --virtual build-dependencies-2 \
        cmake \
        gcc \
        abuild \
        binutils \
        extra-cmake-modules \
        openssl \
        openssl-dev

# Fresh libssh is required to prevent issues like https://github.com/tmate-io/tmate-slave/issues/52
RUN git clone https://git.libssh.org/projects/libssh.git libssh \
    && cd libssh \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DWITH_EXAMPLES=OFF -DWITH_SFTP=OFF .. \
    && make \
    && make install

# Required to show tmate config as we need to extract public key in MD5 format
RUN apk add --no-cache \
        openssh-keygen

RUN cd /src/tmate-server \
    && ./autogen.sh \
    && ./configure CFLAGS="-D_GNU_SOURCE" \
    && make -j \
    && cp tmate-slave /bin/tmate-slave \
    && /bin/sh /tmp/message.sh \
    && apk del build-dependencies \
    && apk del build-dependencies-2

CMD ["/tmate-slave.sh"]
